module.exports = {

    //Hier wel next: als de input valide is wordt het uitgevoerd. Als de input niet valide is moet het naar de error gaan
    validate: function(req, res, next){
        //Dit wordt later een inlog systeem
        const loginCode = req.headers['logintoken'];
        if(!loginCode || loginCode === null || loginCode === 0){
            //Geen toegang
            const errorObject = {
                message: 'Verboden toegang!',
                code: 403
            }
        }else{
        next();
        }
    }
}