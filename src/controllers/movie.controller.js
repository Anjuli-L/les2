const database = require('../database');
const logger = require('../config/appconfig').logger;

module.exports = {
    
    //om alle films op te halen
    //Hierin hoeft geen next te komen omdat dit eigenlijk niet mis kan gaan. Als het mis kan gaan moet je een next toevoegen
    // Zodat je naar de Error handler gaat
    getAllMovies: (req, res) => {
        logger.info('Get /api/movies aangeroepen')
        res.status(200).json({results: database.movies});
    },

    //Deze heeft wel een next nodig voor het geval dat iets niet goed gaat, zoals movie bestaat niet
    getMovieById: function(req, res, next){
        const id = req.params.movieId
        if (id >= 0 && movieId < database.movies.length ){
        const movie = database.movies[movieId];
        res.status(200).json({result: movie})
        }else {
          next()
        }
    },

    createMovie: function(req, res){
        //Iets doen met de body van de request
    const movie = req.body;
    console.log('req body = ', movie);
    database.movies.push(movie);
    res.set(200).json({result: movie});

    }


}
