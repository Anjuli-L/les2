const express = require('express')
const router = express.Router();
const MovieController = require('../controllers/movie.controller')
const AuhtController = require('../controllers/auth.controller')

//Alle films ophalen op de url locatie /
router.get("/", MovieController.getAllMovies)

//authenticator aanpassen en toevoegen, post werkt niet met dat
router.post("/", MovieController.createMovie)

router.get("/:movieId", MovieController.getMovieById)

router.put("/:movieId", (req, res, next)=> {
    const movieId = req.params.movieId
    const movie = database.movies[movieId]
    if(database.movies[movieId] =! null){
        database.movies[movieId] = movie
    }else {
        const errorObj = {
            message: "Movie with ID: " + movieId + " not found",
            code: 404
        }
        next(errorObj)
    }
    console.log('req body =', movie)
})

router.delete("/:movieId", (req, res) => {
    const id = req.params.movieId
    database.movies[id] = null
})

module.exports = router