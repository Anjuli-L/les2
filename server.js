const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./src/config/appconfig').logger;

//Hierdoor koppel je alle beschikbare methods van express aan de const app
const app = express();
const port = process.env.PORT || 3000;

//routes initialiseren
const movieRoutes = require('./src/routes/movie.routes')
const registrationRoutes = require('./src/routes/registration.router')


app.use(bodyParser.json());


//Geeft aan dat alles op deze manier afgehandeld wordt.
app.all("*", (req, res, next) =>
{
  console.log('Generieke afhandeling')
  next();
})


//Function 
let myCallbackFunction = function(){
    console.log('De server luistert op port ' + port)
}

//lambda notatie: () => {}

app.listen(port, myCallbackFunction());
//soort van overerving, koppeling naar movies.routes.js
app.use('/api/movies', movieRoutes)
app.use('/registration', registrationRoutes)