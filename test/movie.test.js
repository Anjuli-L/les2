const database = require('../src/database')
const assert = require('assert')

describe ('MovieDatabase', () =>{

    //it van it should .... Eerst komt de beschrijving en dan de functie () => die beschreven wordt in de {}
    it("should save a movie in the database", done => {

        //zet film in de database
        //Verwachting als het gelukt is dan is database[0] = movie

        const movie = {
            title: "Finding Nemo",
            year: 2009
        }
        database.movies.push(movie)

        //alles testen wat er te testen is wil Robin 
        assert(database.movies[0].title === "Finding Nemo")
        assert(database.movies[0].year === 2009)

        done()
        //Gaat dan naar de volgende it
    })
})